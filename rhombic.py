from sage import *

##

def flatten(l):
    return [item for sublist in l for item in sublist]

# golden_ratio = float((1 + sqrt(5)) / 2)
# golden_ratio = sqrt(2)

p1 = [
    (-1, -1, -1),
    (-1, -1, 1),
    (-1, 1, -1),
    (-1, 1, 1),
    (1, -1, -1),
    (1, -1, 1),
    (1, 1, -1),
    (1, 1, 1),
]

vector_permutations = list(map(lambda g: [ x-1 for x in g.tuple() ], list(CyclicPermutationGroup(3))))

p2 = [
    vector([ 0, golden_ratio, 1/golden_ratio ]),
    vector([ 0, golden_ratio, -1/golden_ratio ]),
    vector([ 0, -golden_ratio, 1/golden_ratio ]),
    vector([ 0, -golden_ratio, -1/golden_ratio ]),
]


p3 = [
    vector([ 0, 1, golden_ratio]),
    vector([ 0, 1, -golden_ratio]),
    vector([ 0, -1, golden_ratio]),
    vector([ 0, -1, -golden_ratio]),
]

p2_expanded = flatten([[(p[e1], p[e2], p[e3]) for (e1, e2, e3) in vector_permutations] for p in p2])
p3_expanded = flatten([[(p[e1], p[e2], p[e3]) for (e1, e2, e3) in vector_permutations] for p in p3])

points = flatten([p1, p2_expanded, p3_expanded])

with open("test.obj", mode='w') as f:
    f.write(point(points).obj())

##

side_length = sqrt(1 + 0 + (-1 / golden_ratio) ** 2)

b_length = sqrt(1 + golden_ratio**2 + 0)
c_length = sqrt(0 + golden_ratio**2 + (1/golden_ratio) ** 2)

scale = 2
(side_length * scale, b_length * scale, c_length * scale)
