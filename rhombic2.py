from sympy.solvers import solve
from sympy.solvers.solveset import nonlinsolve
from sympy.vector import *

from sympy import *

##

a = Symbol('a')
d = Symbol('d')
h = Symbol('h')
d_3 = Symbol('d_3')
q = None


##

h_vec = Matrix([0,0,h])

p1 = Matrix([d/2, 0, h])
p2 = Matrix([0, -h, h])
p3 = Matrix([d_3/2, 0, 0])
p4 = Matrix([d/2, -h, 0])

p1_p2 = (p1-p2).norm()
p1_p3 = (p1-p3).norm()
p2_p4 = (p2-p4).norm()
p2_hvec = (p2-h_vec).norm()
# p3_p4 = (p3-p4).norm()
display("p1_p2", p1_p2)
display("p1_p3", p1_p3)
display("p2_p4", p2_p4)
display("p3_p4", p3_p4)

display("p2_norm", p2.norm())
display("p3_norm", p3.norm())

##

eq1 = sqrt( (d)**2/4 + h**2 ) - a
eq2 = sqrt(h**2 + (d/2 - d_3/2)**2) - a
eq3 = sqrt(d**2/4 + h**2) - a
eq4 = sqrt(2)*h - d_3/2
# eq4 = sqrt(h**2 + d/2-d_3/2) - a
# eq5 = sqrt(h**2 + (d/2)**2) - a


sovle_result = solve([eq1, eq2, eq3, eq4], [d, h, d_3], dict=true)

display(sovle_result)

##

mapping = sovle_result[2]

full_mapping = {}

side_len = 3

full_mapping = {var: float(mapping[var].subs({a: side_len})) for var in [d, h, d_3]}

print(full_mapping)

for var in [d, h, d_3]:
    print(var, float(mapping[var].subs({a: side_len})))


p5 = Matrix([d/2, 0, h])
p6 = Matrix([0, h, h])


points = [p.subs(full_mapping) for p in [p1, p2, p3, p4, p5, p6]]

for p in points:
    print(p)


print("short: ", float(p5.norm().subs(full_mapping)))
print("long: ", float(p6.norm().subs(full_mapping)))


# ##
# 
# 
# solve(
#     [
#         (p1-p2).norm() - a,
#         (p1-p3).norm() - a,
#         (p2-p3).norm() - a,
#         (p2-p4).norm() - a,
#     ],
#     [d,q,h,d_3],
#     dict=True
# )
# 
